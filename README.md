# BetterScripts

A Node parser for the BetterScripts mini-language.

## This project has moved

`better-scripts` now lives in the [`efc` monorepo](https://gitlab.com/eternalfest/efc).
