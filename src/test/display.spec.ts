import chai from "chai";

import { SpanStyle, TextMap } from "../lib/display.js";
import { Span } from "../lib/text.js";

interface LineInfo {
  line: string;
  printable: string;
  width: number;
}

const TEXT: LineInfo[] = [
  {
    line:       "Example text",
    printable:  "Example text",
    width: 12,
  },
  {
    line:       "With\ttabul\tations,",
    printable:  "With    tabul   ations,",
    width: 23,
  },
  {
    line:       "control \x00\x11\x07 characters",
    printable:  "control  characters",
    width: 19,
  },
  {
    line:       "and astral plane thingies 🙐.",
    printable:  "and astral plane thingies 🙐.",
    width: 28,
  },
  {
    line:       "The End",
    printable:  "The End",
    width: 7,
  },
];

const LONG_TEXT: string[] = [
  "A123456789A12345678", // 20 chars per line, counting the \n
  "B123456789B12345678",
  "C123456789C12345678",
  "D123456789D12345678",
  "E123456789E12345678",
  "F123456789F12345678",
  "G123456789G12345678",
  "H123456789H12345678",
  "I123456789I12345678",
  "J123456789J12345678", // 10 lines
];

interface SpanAnnotations {
  context: number;
  spans: [number, number, SpanStyle][];
  result: string[];
}

const ANNOTATED: SpanAnnotations[] = [
  {
    context: 2,
    spans: [[5, 170, SpanStyle.Double], [10, 15, SpanStyle.Heavy], [3, 12, SpanStyle.Light]],
    result: [
      "0    : A123456789A12345678",
      "   ╔════════^    ▲▲▲▲▲"    ,
      "   ║      ~~~~~~~~~"       ,
      "1  ║ : B123456789B12345678",
      "2  ║ : C123456789C12345678",
      "   ║   ..."                ,
      "7  ║ : H123456789H12345678",
      "8  ║ : I123456789I12345678",
      "   ╚════════════^"         ,
      "9    : J123456789J12345678",
    ],
  },
  {
    context: 0,
    spans: [
      [110, 119, SpanStyle.Double], [102, 107, SpanStyle.Dotted],
      [5, 85, SpanStyle.Light], [25, 105, SpanStyle.Heavy], [45, 125, SpanStyle.Double],
      [65, 145, SpanStyle.Curved], [85, 165, SpanStyle.Dotted], [105, 185, SpanStyle.Double],
    ],
    result: [
      "0       : A123456789A12345678",
      "   ┌───────────^"             ,
      "1  │    : B123456789B12345678",
      "   │┏━━━━━━━━━━^"             ,
      "2  │┃   : C123456789C12345678",
      "   │┃╔═════════^"             ,
      "3  │┃║  : D123456789D12345678",
      "   │┃║╭────────^"             ,
      "4  │┃║│ : E123456789E12345678",
      "   └╂╫┼───────^"              ,
      "   ┌╂╫┼╌╌╌╌╌╌╌╌^"             ,
      "5  ╎┃║│ : F123456789F12345678",
      "   ╎┗╫┿━━━━━━━^     ^^^^^^^^^",
      "   ╎╔╬╪════════^"             ,
      "   ╎║║│     -----"            ,
      "6  ╎║║│ : G123456789G12345678",
      "   ╎║╚╪═══════^"              ,
      "7  ╎║ │ : H123456789H12345678",
      "   ╎║ ╰───────^"              ,
      "8  ╎║   : I123456789I12345678",
      "   └╫╌╌╌╌╌╌╌╌╌^"              ,
      "9   ║   : J123456789J12345678",
      "    ╚═════════^"              ,
    ],
  },
];

describe("textmap", () => {
  const text = TEXT.map(t => t.line).join("\n");
  const map = new TextMap(text, 4);

  it("correctly split text into lines", () => {
    for (let i = 0; i < TEXT.length; i++) {
      chai.assert.equal(map.getLine(i), TEXT[i].line);
    }
    chai.assert.isUndefined(map.getLine(TEXT.length));
  });

  it("correctly calculate positions", () => {
    for (let i = 0; i < TEXT.length; i++) {
      const span = map.getLineSpan(i);
      chai.assert.isDefined(span);
      chai.assert.deepEqual(map.posOf(span!.start), { line: i, col: 0 });
      chai.assert.deepEqual(map.posOf(span!.end), { line: i, col: TEXT[i].width });
    }
  });

  it("correctly formats printable lines", () => {
    for (let i = 0; i < TEXT.length; i++) {
      chai.assert.equal(map.getPrintableLine(i), TEXT[i].printable);
    }
    chai.assert.isUndefined(map.getPrintableLine(TEXT.length));
  });

  describe("span annotation", () => {
    const map = new TextMap(LONG_TEXT.join("\n"), 4);
    for (let i = 0; i < ANNOTATED.length; i++) {
      const cur = ANNOTATED[i];
      it(`correctly annotate spans for #${i}`, () => {
        const spans = cur.spans.map(s => {
          return Span.of(s[0], s[1]).with(s[2]);
        });
        const actual = map.getSpansInContext(cur.context, ...spans);
        const expected = cur.result.join("\n") + "\n";
        chai.assert.strictEqual(actual, expected);
      });
    }

    it("returns an empty string if array is empty", () => {
      chai.assert.strictEqual(map.getSpansInContext(2), "");
      chai.assert.strictEqual(map.getSpansInContext(2, {data: SpanStyle.Light}), "");
    });

    it("rejects invalid lineContext", () => {
      chai.assert.throws(() => map.getSpansInContext(-1));
    });
  });
});
