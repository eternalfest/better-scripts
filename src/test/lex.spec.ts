import chai from "chai";

import { Lexer, Token, unescapeString } from "../lib/lex.js";
import { Span, Spanned } from "../lib/text.js";

interface TestCase {
    contents: string;
    tokens: [Token, number, number][];
}

const TEST_CASES: TestCase[] = [
  {
    contents: "",
    tokens: [],
  },
  {
    contents: "foo bar baz quux true false",
    tokens: [
      [Token.Ident, 0, 3], [Token.Ident, 4, 7], [Token.Ident, 8, 11],
      [Token.Ident, 12, 16], [Token.LitBoolTrue, 17, 21], [Token.LitBoolFalse, 22, 27],
    ],
  },
  {
    contents: "\"foo\" '\"bâr\"' '\\\\nbaz\\t' ''",
    tokens: [[Token.LitString, 0, 5], [Token.LitString, 6, 13], [Token.LitString, 14, 24], [Token.LitString, 25, 27]],
  },
  {
    contents: "00 1  5432  42 1.0  .2 12e34 0.123e-01  999e+999",
    tokens: [
      [Token.LitNumber, 0, 2], [Token.LitNumber, 3, 4], [Token.LitNumber, 6, 10],
      [Token.LitNumber, 12, 14], [Token.LitNumber, 15, 18], [Token.LitNumber, 20, 22],
      [Token.LitNumber, 23, 28], [Token.LitNumber, 29, 38], [Token.LitNumber, 40, 48],
    ],
  },
  {
    contents: "( )[]{ }  ,; === +=-=/=*=%=|=&=^= !=>=<=>< &&||! +-*/%&|^~ .",
    tokens: [
      [Token.LParen, 0, 1], [Token.RParen, 2, 3], [Token.LBracket, 3, 4],
      [Token.RBracket, 4, 5], [Token.LBrace, 5, 6], [Token.RBrace, 7, 8],
      [Token.Comma, 10, 11], [Token.Semicolon, 11, 12], [Token.Equals, 13, 15],
      [Token.Assign, 15, 16], [Token.AddAssign, 17, 19], [Token.SubAssign, 19, 21],
      [Token.DivAssign, 21, 23], [Token.MulAssign, 23, 25], [Token.ModAssign, 25, 27],
      [Token.OrAssign, 27, 29], [Token.AndAssign, 29, 31], [Token.XorAssign, 31, 33],
      [Token.NotEquals, 34, 36], [Token.GreaterThanEquals, 36, 38], [Token.LesserThanEquals, 38, 40],
      [Token.GreaterThan, 40, 41], [Token.LesserThan, 41, 42], [Token.LogicalAnd, 43, 45],
      [Token.LogicalOr, 45, 47], [Token.LogicalNot, 47, 48], [Token.Add, 49, 50],
      [Token.Sub, 50, 51], [Token.Mul, 51, 52], [Token.Div, 52, 53],
      [Token.Mod, 53, 54], [Token.BitwiseAnd, 54, 55], [Token.BitwiseOr, 55, 56],
      [Token.BitwiseXor, 56, 57], [Token.BitwiseNot, 57, 58], [Token.Dot, 59, 60],
    ],
  },
  {
    contents: `
      foo
      //comment /*
      bar
      /* comment //*/
      baz/**/quux
    `,
    tokens: [[Token.Ident, 7, 10], [Token.Ident, 36, 39], [Token.Ident, 68, 71], [Token.Ident, 75, 79]],
  },
];

const INVALID_TEST_CASES: string[] = [
  "invalid char £",
  "/* unterminated comment",
  "'unterminated string",
  "'\t string with control char'",
  "malformed number .1.2",
  "malformed number 1..2",
  "malformed number 0.e12",
  "malformed number 12ee3",
  "malformed number 1.2z",
  "malformed number 1.2e",
];

const STRING_UNESCAPE_CASES: string[][] = [
  ["foo", "foo"],
  ["\\'", "'"],
  ["\\b\\f a\\\\a\\n \\r\\t", "\b\f a\\a\n \r\t"],
];

function makeToken([tok, start, end]: [Token, number, number]): Spanned<Token> {
  return {
    data: tok,
    span: Span.of(start, end),
  };
}

describe("lex", () => {
  describe("string unescaping", () => {
    for (const [escaped, expected] of STRING_UNESCAPE_CASES) {
      it(`unescape string: ${escaped}`, () => chai.assert.equal(expected, unescapeString(escaped)));
    }
  });

  it("next(), peek() and pos() work correctly", () => {
    const lexer = new Lexer("{ [ (");
    chai.assert.equal(0, lexer.pos());
    chai.assert.equal(Token.LBrace, lexer.peek().data);
    chai.assert.equal(Token.LBrace, lexer.next().data);
    chai.assert.equal(1, lexer.pos());
    chai.assert.equal(Token.LBracket, lexer.next().data);
    chai.assert.equal(3, lexer.pos());
    chai.assert.equal(Token.LParen, lexer.peek().data);
    chai.assert.equal(3, lexer.pos());
    chai.assert.equal(Token.LParen, lexer.peek().data);
    chai.assert.equal(Token.LParen, lexer.next().data);
    chai.assert.equal(5, lexer.pos());
    chai.assert.equal(Token.EOF, lexer.next().data);
    chai.assert.equal(Token.EOF, lexer.next().data);
    chai.assert.equal(5, lexer.pos());
  });

  describe("test data", () => {
    for (const { contents, tokens } of TEST_CASES) {
      const lexer = new Lexer(contents);
      it(`lexes the string: ${contents}`, () => {
        for (const tok of tokens!) {
          chai.assert.deepEqual(makeToken(tok), lexer.next(), "unexpected token");
        }
        const eof = makeToken([Token.EOF, contents.length, contents.length]);
        chai.assert.deepEqual(eof, lexer.next(), "unexpected token");
      });
    }

    for (const contents of INVALID_TEST_CASES) {
      const lexer = new Lexer(contents);
      it(`errors on invalid string: ${contents}`, () => {
        chai.assert.throws(() => {
          while (lexer.next().data !== Token.EOF) {
            // consume the token
          }
        });
      });
    }
  });
});
