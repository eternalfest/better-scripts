import chai from "chai";
import fs from "fs";
import path from "path";

import { parse } from "../lib/index.js";
import {dirname} from "./meta.js";

const SCRIPT_EXT: string = ".script";
const JSON_EXT: string = ".json";
const SCRIPT_DIR: string = path.resolve(dirname, "test-resources/script-data");

function getScriptsInDir(dirname: string): string[] {
  return fs.readdirSync(dirname, { withFileTypes: true })
    .filter(f => f.isFile() && f.name.endsWith(SCRIPT_EXT))
    .map(f => f.name.substring(0, f.name.length - SCRIPT_EXT.length));
}

describe("parse", () => {
  for (const name of getScriptsInDir(SCRIPT_DIR)) {
    const input: string = fs.readFileSync(path.resolve(SCRIPT_DIR, name + SCRIPT_EXT)).toString();
    const expected: any = JSON.parse(fs.readFileSync(path.resolve(SCRIPT_DIR, name + JSON_EXT)).toString());
    const expectedErrors = expected.errors;
    const expectedScript = expected.script;
    const hasErrors = expectedErrors.length > 0;

    it(`${hasErrors ? "errors on" : "parses the"} script '${name}'`, () => {

      const result = parse(input);
      const actualErrors = result.errors.map(e => e.msg);
      const actualScript = result.script.export();

      chai.assert.deepStrictEqual(actualErrors, expectedErrors);
      chai.assert.deepStrictEqual(actualScript, expectedScript);
    });
  }
});
