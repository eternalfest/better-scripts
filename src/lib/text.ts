
// Represent a span in the source string
export class Span {
  public readonly start: number;
  public readonly end: number;

  // @ts-ignore allow unused variable
  private __noDuckTyping: undefined;

  constructor(start: number, end: number) {
    if (start < 0 || Math.floor(start) !== start || start > Number.MAX_SAFE_INTEGER) {
      throw new TypeError(`start must be a non-negative integer (is ${start}`);
    } else if (end < 0 || Math.floor(end) !== end || end > Number.MAX_SAFE_INTEGER) {
      throw new TypeError(`end must be a non-negative integer (is ${end}`);
    } else if (start > end) {
      throw new TypeError(`start cannot be greater than end (${start} > ${end})`);
    }
    this.start = start;
    this.end = end;
  }

  public static of(start: number, end: number): Span {
    return new Span(start, end);
  }

  public with<T>(data: T): Spanned<T> {
    return { data, span: this };
  }
}

// An object with an optional span
export interface Spanned<T> {
  readonly data: T;
  readonly span?: Span;
}
