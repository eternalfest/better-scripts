
import { Incident } from "incident";

import { ErrorType, makeScriptIncident, ScriptError } from "./error.js";
import { Span, Spanned } from "./text.js";

export interface TokenFamily<T> {
  readonly name: string;
  readonly tokens: ReadonlyMap<Token, T>;
}

export enum Token {
  LitNumber,
  LitString,
  LitBoolTrue,
  LitBoolFalse,
  Ident,

  LParen,
  RParen,
  LBracket,
  RBracket,
  LBrace,
  RBrace,

  Comma,
  Semicolon,
  Dot,

  AddAssign,
  SubAssign,
  DivAssign,
  MulAssign,
  ModAssign,
  OrAssign,
  AndAssign,
  XorAssign,

  Equals,
  NotEquals,
  GreaterThanEquals,
  LesserThanEquals,
  GreaterThan,
  LesserThan,
  LogicalOr,
  LogicalAnd,
  LogicalNot,

  Add,
  Sub,
  Mul,
  Div,
  Mod,

  BitwiseOr,
  BitwiseAnd,
  BitwiseXor,
  BitwiseNot,

  Assign,

  EOF,
}

export const TOKENS_LITERAL: TokenFamily<null> = {
  name: "literal",
  tokens: makeMap(Token.LitNumber, Token.LitString, Token.LitBoolTrue, Token.LitBoolFalse),
};

export const TOKENS_ASSIGN: TokenFamily<null> = {
  name: "assign op.",
  tokens: makeMap(
    Token.Assign, Token.AddAssign, Token.SubAssign,
    Token.MulAssign, Token.DivAssign, Token.ModAssign,
    Token.AndAssign, Token.OrAssign, Token.XorAssign,
  ),
};

export const TOKENS_PREFIX: TokenFamily<null> = {
  name: "prefix op.",
  tokens: makeMap(Token.Sub, Token.LogicalNot, Token.BitwiseNot),
};

export const TOKENS_INFIX: TokenFamily<number> = {
  name: "infix op.",
  tokens: new Map([
    [Token.Mul, 300],
    [Token.Div, 300],
    [Token.Mod, 300],

    [Token.Add, 200],
    [Token.Sub, 200],

    [Token.GreaterThan, 100],
    [Token.LesserThan, 100],
    [Token.GreaterThanEquals, 100],
    [Token.LesserThanEquals, 100],
    [Token.Equals, 90],
    [Token.NotEquals, 90],

    [Token.BitwiseAnd, 83],
    [Token.BitwiseXor, 82],
    [Token.BitwiseOr, 81],

    [Token.LogicalAnd, 70],
    [Token.LogicalOr, 60],
  ]),
};

function makeMap<T>(...elems: T[]): Map<T, null> {
  const map = new Map();
  for (const e of elems) {
    map.set(e, null);
  }
  return map;
}

const TOKEN_NAMES: Map<Token, string> = new Map([
  [Token.LitNumber, "number"],
  [Token.LitString, "string"],
  [Token.LitBoolFalse, "'false'"],
  [Token.LitBoolTrue, "'true'"],
  [Token.Ident, "identifier"],
  [Token.LParen, "'('"],
  [Token.RParen, "')'"],
  [Token.LBracket, "'['"],
  [Token.RBracket, "']'"],
  [Token.LBrace, "'{'"],
  [Token.RBrace, "'}'"],
  [Token.Comma, "','"],
  [Token.Semicolon, "';'"],
  [Token.Dot, "'.'"],
  [Token.Assign, "'='"],
  [Token.AddAssign, "'+='"],
  [Token.SubAssign, "'-='"],
  [Token.DivAssign, "'/='"],
  [Token.MulAssign, "'*='"],
  [Token.ModAssign, "'%='"],
  [Token.OrAssign, "'|='"],
  [Token.AndAssign, "'&='"],
  [Token.XorAssign, "'^='"],
  [Token.Equals, "'=='"],
  [Token.NotEquals, "'!='"],
  [Token.GreaterThanEquals, "'>='"],
  [Token.LesserThanEquals, "'<='"],
  [Token.GreaterThan, "'>'"],
  [Token.LesserThan, "'<'"],
  [Token.LogicalOr, "'||'"],
  [Token.LogicalAnd, "'&&'"],
  [Token.LogicalNot, "'!'"],
  [Token.Add, "'+'"],
  [Token.Sub, "'-'"],
  [Token.Mul, "'*'"],
  [Token.Div, "'/'"],
  [Token.Mod, "'%'"],
  [Token.BitwiseOr, "'|'"],
  [Token.BitwiseAnd, "'&'"],
  [Token.BitwiseXor, "'^'"],
  [Token.BitwiseNot, "'~'"],
  [Token.EOF, "EOF"],
]);

export function getTokenName(tok: Token): string {
  return TOKEN_NAMES.get(tok)!;
}

const STRING_ESCAPES: Map<string, string> = new Map([
  ["b", "\b"],
  ["f", "\f"],
  ["n", "\n"],
  ["r", "\r"],
  ["t", "\t"],
]);

export function unescapeString(str: string): string {
  const firstBackslash = str.indexOf("\\");
  if (firstBackslash < 0) {
    return str;
  }

  let res = "";
  let backslash = false;
  for (let i = 0; i < str.length; i++) {
    const c = str.charAt(i);
    if (backslash) {
      const unescape = STRING_ESCAPES.get(c);
      res += unescape !== undefined ? unescape : c;
      backslash = false;
    } else if (c === "\\") {
      backslash = true;
    } else {
      res += c;
    }
  }

  return res;
}

export class Lexer {
  readonly lexer: LexerBase;
  nextTok: Spanned<Token> | undefined;
  curPos: number;

  constructor(contents: string) {
    this.lexer = new LexerBase(contents);
    this.nextTok = undefined;
    this.curPos = 0;
  }

  public next(): Spanned<Token> {
    let tok;
    if (this.nextTok === undefined) {
      tok = this.lexer.nextTok();
    } else {
      tok = this.nextTok;
      this.nextTok = undefined;
    }

    this.curPos = tok.span!.end;
    return tok;
  }

  public peek(): Spanned<Token> {
    if (this.nextTok === undefined) {
      this.nextTok = this.lexer.nextTok();
    }
    return this.nextTok;
  }

  public pos(): number {
    return this.curPos;
  }
}

class LexerBase {
  readonly contents: string;
  cursor: number;

  constructor(contents: string) {
    this.contents = contents;
    this.cursor = 0;
  }

  public isDone(): boolean {
    return this.cursor >= this.contents.length;
  }

  public nextTok(): Spanned<Token> {
    for (;;) {
      const start = this.cursor;
      const tok = this.nextTokRaw();
      const end = this.cursor;
      if (tok !== undefined) {
        return {
          data: tok,
          span: Span.of(start, end),
        };
      }
    }
  }

  // Returns the next token, or undefined if it is whitespace or a commment
  private nextTokRaw(): Token | undefined {
    const c = this.nextChar();
    if (chars.isWhite(c)) {
      return this.parseWhite();
    } else if (chars.isDigit(c)) {
      return this.parseNumber(c);
    } else if (chars.isIdentStart(c)) {
      return this.parseIdent();
    }

    switch (c) {
      case chars.DOT:
        if (chars.isDigit(this.peekChar())) {
          return this.parseNumber(c);
        }
        return Token.Dot;
      case chars.SINGLE_QUOTE:
      case chars.DOUBLE_QUOTE:
        return this.parseString(c);
      case chars.SLASH:
        if (this.advanceCharIf(chars.EQUALS)) {
          return Token.DivAssign;
        } else if (this.advanceCharIf(chars.SLASH)) {
          return this.parseSingleLineComment();
        } else if (this.advanceCharIf(chars.ASTERISK)) {
          return this.parseMultiLineComment();
        } else {
          return Token.Div;
        }
      case chars.L_PAREN:
        return Token.LParen;
      case chars.R_PAREN:
        return Token.RParen;
      case chars.L_BRACKET:
        return Token.LBracket;
      case chars.R_BRACKET:
        return Token.RBracket;
      case chars.L_BRACE:
        return Token.LBrace;
      case chars.R_BRACE:
        return Token.RBrace;
      case chars.COMMA:
        return Token.Comma;
      case chars.SEMICOLON:
        return Token.Semicolon;
      case chars.PLUS:
        return this.advanceCharIf(chars.EQUALS) ? Token.AddAssign : Token.Add;
      case chars.MINUS:
        return this.advanceCharIf(chars.EQUALS) ? Token.SubAssign : Token.Sub;
      case chars.ASTERISK:
        return this.advanceCharIf(chars.EQUALS) ? Token.MulAssign : Token.Mul;
      case chars.PERCENT:
        return this.advanceCharIf(chars.EQUALS) ? Token.ModAssign : Token.Mod;
      case chars.CARET:
        return this.advanceCharIf(chars.EQUALS) ? Token.XorAssign : Token.BitwiseXor;
      case chars.EXCLAMATION_PT:
        return this.advanceCharIf(chars.EQUALS) ? Token.NotEquals : Token.LogicalNot;
      case chars.GREATER_THAN:
        return this.advanceCharIf(chars.EQUALS) ? Token.GreaterThanEquals : Token.GreaterThan;
      case chars.LESSER_THAN:
        return this.advanceCharIf(chars.EQUALS) ? Token.LesserThanEquals : Token.LesserThan;
      case chars.PIPE:
        if (this.advanceCharIf(chars.EQUALS)) {
          return Token.OrAssign;
        } if (this.advanceCharIf(chars.PIPE)) {
          return Token.LogicalOr;
        } else {
          return Token.BitwiseOr;
        }
      case chars.AMPERSAND:
        if (this.advanceCharIf(chars.EQUALS)) {
          return Token.AndAssign;
        } else if (this.advanceCharIf(chars.AMPERSAND)) {
          return Token.LogicalAnd;
        } else {
          return Token.BitwiseAnd;
        }
      case chars.TILDE:
        return Token.BitwiseNot;
      case chars.EQUALS:
        return this.advanceCharIf(chars.EQUALS) ? Token.Equals : Token.Assign;
      case -1:
        return Token.EOF;
      default:
        this.cursor--;
        throw this.makeError();
    }
  }

  private parseString(quote: number): Token {
    let escaped = false;
    for (;;) {
      const c = this.nextChar();

      if (c === -1) {
        throw this.makeError("Unexpected end of string in string literal");
      } else if (c < chars.SPACE) {
        this.cursor--;
        throw this.makeError("Unexpected character in string literal");
      }

      if (escaped) {
        escaped = false;
        continue;
      }

      if (c === chars.ANTISLASH) {
        escaped = true;
      } else if (c === quote) {
        return Token.LitString;
      }
    }
  }

  private parseNumber(first: number): Token {
    const foundPoint = first === chars.DOT;
    let foundDigits = chars.isDigit(first);

    // Integer part (or fractional part if started by a dot)
    let c = this.peekChar();
    while (chars.isDigit(c)) {
      foundDigits = true;
      c = this.advanceChar();
    }

    if (c === chars.DOT) {
      // Fractional part
      if (foundPoint) {
        throw this.makeError();
      }

      c = this.advanceChar();
      if (!chars.isDigit(c)) {
        throw this.makeError();
      }

      foundDigits = true;
      while (chars.isDigit(c)) {
        c = this.advanceChar();
      }
    }

    if (!foundDigits) {
      throw this.makeError();
    }

    if (c === chars.LOWER_E || c === chars.UPPER_E) {
      // Exponent part
      c = this.advanceChar();
      if (c === chars.PLUS || c === chars.MINUS) {
        c = this.advanceChar();
      } else if (!chars.isDigit(c)) {
        throw this.makeError();
      }

      while (chars.isDigit(c)) {
        c = this.advanceChar();
      }
    }

    if (chars.isIdentContinue(c) || c === chars.DOT) {
      throw this.makeError();
    }

    return Token.LitNumber;
  }

  private parseIdent(): Token {
    const start = this.cursor - 1;
    while (chars.isIdentContinue(this.peekChar())) {
      this.cursor++;
    }
    const end = this.cursor;

    if ((end - start === 4) && this.contents.substring(start, end) === "true") {
      return Token.LitBoolTrue;
    } else if ((end - start === 5) && this.contents.substring(start, end) === "false") {
      return Token.LitBoolFalse;
    }
    return Token.Ident;
  }

  private parseWhite(): undefined {
    while (chars.isWhite(this.peekChar())) {
      this.cursor++;
    }
    return;
  }

  private parseSingleLineComment(): undefined {
    let c;
    do {
      c = this.nextChar();
    } while (c !== chars.CR && c !== chars.LF && c !== -1);
    return;
  }

  private parseMultiLineComment(): undefined {
    let c;
    let asterisk = false;
    for (;;) {
      c = this.nextChar();
      if (c === -1) {
        throw this.makeError("Unexpected end of string in comment");
      } else if (asterisk && c === chars.SLASH) {
        return;
      }

      asterisk = c === chars.ASTERISK;
    }
  }

  private peekChar(): number {
    if (this.isDone()) {
      return -1;
    }
    return this.contents.charCodeAt(this.cursor);
  }

  private nextChar(): number {
    if (this.isDone()) {
      return -1;
    }
    const c = this.contents.charCodeAt(this.cursor);
    this.cursor++;
    return c;
  }

  private advanceChar(): number {
    this.cursor++;
    return this.peekChar();
  }

  private advanceCharIf(charCode: number): boolean {
    if (this.peekChar() === charCode) {
      this.cursor++;
      return true;
    }
    return false;
  }

  private makeError(msg: string = "Unexpected character"): Incident<ScriptError> {
    return makeScriptIncident(
      ErrorType.Lexical,
      msg,
      Span.of(this.cursor, this.cursor + 1),
    );
  }
}

namespace chars {
  export const LF: number = 0x0A;
  export const CR: number = 0x0D;
  export const SPACE: number = 0x20;
  export const TAB: number = 0x09;
  export const FORM_FEED: number = 0x0C;

  export const DIGIT_0: number = 0x30;
  export const DIGIT_9: number = 0x39;
  export const UPPER_A: number = 0x41;
  export const UPPER_E: number = 0x45;
  export const UPPER_Z: number = 0x5A;
  export const LOWER_A: number = 0x61;
  export const LOWER_E: number = 0x65;
  export const LOWER_Z: number = 0x7A;

  export const EXCLAMATION_PT: number = 0x21;
  export const SINGLE_QUOTE: number = 0x22;
  export const PERCENT: number = 0x25;
  export const AMPERSAND: number = 0x26;
  export const DOUBLE_QUOTE: number = 0x27;
  export const L_PAREN: number = 0x28;
  export const R_PAREN: number = 0x29;
  export const ASTERISK: number = 0x2A;
  export const PLUS: number = 0x2B;
  export const COMMA: number = 0x2C;
  export const MINUS: number = 0x2D;
  export const DOT: number = 0x2E;
  export const SLASH: number = 0x2F;

  export const SEMICOLON: number = 0x3B;
  export const LESSER_THAN: number = 0x3C;
  export const EQUALS: number = 0x3D;
  export const GREATER_THAN: number = 0x3E;

  export const L_BRACKET: number = 0x5B;
  export const ANTISLASH: number = 0x5C;
  export const R_BRACKET: number = 0x5D;
  export const CARET: number = 0x5E;
  export const UNDERSCORE: number = 0x5F;

  export const L_BRACE: number = 0x7B;
  export const PIPE: number = 0x7C;
  export const R_BRACE: number = 0x7D;
  export const TILDE: number = 0x7E;

  export function isWhite(c: number): boolean {
    return c === LF || c === CR || c === SPACE || c === TAB;
  }

  export function isIdentStart(c: number): boolean {
    return (c >= UPPER_A && c <= UPPER_Z) || (c >= LOWER_A && c <= LOWER_Z) || c === UNDERSCORE;
  }

  export function isIdentContinue(c: number): boolean {
    return isIdentStart(c) || isDigit(c);
  }

  export function isDigit(c: number): boolean {
    return c >= DIGIT_0 && c <= DIGIT_9;
  }
}
