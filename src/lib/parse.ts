import * as ast from "./ast.js";
import { ErrorType, isScriptIncident, makeScriptIncident, ScriptError } from "./error.js";
import { getTokenName, Lexer, Token, TokenFamily, unescapeString } from "./lex.js";
import * as lex from "./lex.js";
import { Span, Spanned } from "./text.js";

/*
EBNF grammar :
  Literal     := <NumberLit> | <StringLit> | <BoolLit>

  VarPart     := ( '.' <Identifier> ) | ( '[' Expression ']' )
  ExprVar     := <Identifier> VariablePart*
  ExprCall    := <Identifier> '(' [Expression (',' Expression)*] ')'
  Expression  := Literal | ExprVariable | ExprCall
               | Expression <BinOp> Expression | <UnaryOp> Expression
               | '(' Expression ')'

  Parameter   := <Identifier> ['=' Expression]
  ParamList   := '(' [(Parameter (',' Parameter)*) | Expression] ')'
  Event       := <Identifier> [ParamList] Parameter* (';' | Block)

  Statement      := ExprVar <AssignOp> Expression ';'

  Node        := Event | Statement
  Block       := '{' Node* '}'

  Script      := Node* <EOF>
*/

export interface ScriptParseResult {
  script: ast.BetterScript;
  errors: ScriptError[];
}

export function parse(text: string): ScriptParseResult {
  const ctx = new Context(text);
  const script = new ast.BetterScript();

  ctx.catchError(null, () => {
    while (ctx.matches(Token.EOF) === undefined) {
      parseNodeInto(ctx, script.nodes);
    }
  });

  return { script, errors: ctx.errors };
}

function parseNodeInto(ctx: Context, nodes: Spanned<ast.ScriptNode>[]): void {
  const nameOrVar = parseExprVariable(ctx, ctx.getString(ctx.consume(Token.Ident).span!));

  const assign = ctx.tryConsumeFamily(lex.TOKENS_ASSIGN);
  if (assign !== undefined) {
    const op = assign.data === Token.Assign ? undefined : getTokenAssignOpName(assign.data);
    const rvalue = parseExpression(ctx);
    ctx.tryConsume(Token.Semicolon);

    nodes.push({
      span: Span.of(nameOrVar.span!.start, ctx.curPos()),
      data: { type: "assign", lvalue: nameOrVar, rvalue, op },
    });

  } else {
    if (nameOrVar.data.parts.length > 0) {
      return ctx.unexpected();
    }
    const node = new ast.ScriptEvent(nameOrVar.data.name);

    try {
      parseEventContents(ctx, node);
    } finally {
      nodes.push({
        span: Span.of(nameOrVar.span!.start, ctx.curPos()),
        data: node,
      });
    }
  }
}

function parseEventContents(ctx: Context, node: ast.ScriptEvent): void {
  // Parameter list
  if (ctx.tryConsume(Token.LParen) !== undefined) {
    if (ctx.tryConsume(Token.RParen) === undefined) {
      let preferAnonParam = true;
      do {
        ctx.catchError(ErrorType.Semantic, () => {
          const param = parseParameter(ctx, preferAnonParam);
          if (param.name.data === "") {
            node.setAnonParameter(param.value);
          } else {
            node.addParameter(param);
          }
        });

        preferAnonParam = false;
      } while (ctx.consume2(Token.Comma, Token.RParen).data === Token.Comma);
    }
  }

  // Modifiers
  while (ctx.matches(Token.Ident) !== undefined) {
    ctx.catchError(ErrorType.Semantic, () => {
      node.addModifier(parseParameter(ctx, null));
    });
  }

  // Child nodes
  const tok = ctx.consume2(Token.LBrace, Token.Semicolon);
  if (tok.data === Token.LBrace) {
    while (ctx.tryConsume(Token.RBrace) === undefined) {
      parseNodeInto(ctx, node.children);
    }
  }
}

// preferAnonParam controls how ambiguities between named
// and anonymous parameters are resolved:
//   - null: anonymous parameters are a syntax error
//   - false: 'foo' is parsed as a named param. set to 'true'
//   - true: 'foo' (and only if last) is parsed as an anonymous param.
function parseParameter(ctx: Context, preferAnonParam: boolean | null): ast.ScriptParameter {
  let name: Spanned<string>;
  let expr: Spanned<ast.ScriptExpr>;
  if (preferAnonParam === null) {
    name = ctx.getString(ctx.consume(Token.Ident).span!);
  } else {
    expr = parseExpression(ctx);
    if (expr.data.type !== "var" || expr.data.parts.length > 0) {
      // Must be a complex expression, return it as anon. param
      return {
        name: { data: "", span: undefined },
        value: expr,
      };
    }

    name = expr.data.name;
  }

  if (ctx.tryConsume(Token.Assign) === undefined) {
    return (preferAnonParam && ctx.matches(Token.RParen) !== undefined) ? {
      name: { data: "", span: undefined },
      value: expr!,
    } : {
      name,
      value: { data: { type: "lit", value: true }, span: undefined },
    };
  }

  return { name, value: parseExpression(ctx) };
}

function parseExpression(ctx: Context, precedence: number = 0): Spanned<ast.ScriptExpr> {
  // Infix operators: this is the precedence climbing algorithm
  let left = parseExprAtom(ctx);

  for (;;) {
    const infix = ctx.matchesFamily(lex.TOKENS_INFIX);
    if (infix === undefined) {
      break;
    }
    const curPrec = lex.TOKENS_INFIX.tokens.get(infix)!;
    if (curPrec < precedence) {
      break;
    }
    ctx.next();
    const op = getTokenOpName(infix);

    // All infix op. are left-associative
    const right = parseExpression(ctx, curPrec + 1);
    left = {
      data: { type: "binop", left, right, op },
      span: Span.of(left.span!.start, ctx.curPos()),
    };
  }
  return left;
}

function parseExprAtom(ctx: Context): Spanned<ast.ScriptExpr> {
  // Parenthesized expressions
  const parenTok = ctx.tryConsume(Token.LParen);
  if (parenTok !== undefined) {
    const expr = parseExpression(ctx);
    ctx.consume(Token.RParen);
    return {
      data: expr.data,
      span: Span.of(parenTok.span!.start, ctx.curPos()),
    };
  }

  // Prefix operators
  const prefixTok = ctx.tryConsumeFamily(lex.TOKENS_PREFIX);
  if (prefixTok !== undefined) {
    const expr = parseExprAtom(ctx);

    let data: ast.ScriptExpr;
    // Special case for negative numbers
    if (prefixTok.data === Token.Sub && expr.data.type === "lit" && typeof expr.data.value === "number") {
      data = { type: "lit", value: -expr.data.value };
    } else {
      const op = getTokenOpName(prefixTok.data);
      data = { type: "unaryop", op, expr };
    }

    return { data, span: Span.of(prefixTok.span!.start, ctx.curPos()) };
  }

  // Variables and function calls
  const nameTok = ctx.tryConsume(Token.Ident);
  if (nameTok !== undefined) {
    const name = ctx.getString(nameTok.span!);
    return ctx.matches(Token.LParen) === undefined ? parseExprVariable(ctx, name) : parseExprCall(ctx, name);
  }

  // Literals
  return parseLiteral(ctx);
}

function parseExprVariable(ctx: Context, name: Spanned<string>): Spanned<ast.ScriptExprVar> {
  const parts: Spanned<string | ast.ScriptExpr>[] = [];

  for (;;) {
    if (ctx.tryConsume(Token.Dot) !== undefined) {
      parts.push(ctx.getString(ctx.consume(Token.Ident).span!));
    } else if (ctx.tryConsume(Token.LBracket) !== undefined) {
      parts.push(parseExpression(ctx));
      ctx.consume(Token.RBracket);
    } else {
      return {
        data: { type: "var", name, parts },
        span: Span.of(name.span!.start, ctx.curPos()),
      };
    }
  }
}

function parseExprCall(ctx: Context, func: Spanned<string>): Spanned<ast.ScriptExprCall> {
  const args: Spanned<ast.ScriptExpr>[] = [];
  ctx.consume(Token.LParen);
  if (ctx.tryConsume(Token.RParen) === undefined) {
    do {
      args.push(parseExpression(ctx));
    } while (ctx.tryConsume(Token.Comma) !== undefined);
    ctx.consume(Token.RParen);
  }

  return {
    data: { type: "call", func: func.data, args },
    span: Span.of(func.span!.start, ctx.curPos()),
  };
}

function parseLiteral(ctx: Context): Spanned<ast.ScriptValue> {
  const tok = ctx.consumeFamily(lex.TOKENS_LITERAL);
  switch (tok.data) {
    case Token.LitBoolFalse:
      return { data: { type: "lit", value: false }, span: tok.span };
    case Token.LitBoolTrue:
      return { data: { type: "lit", value: true }, span: tok.span };
    case Token.LitNumber:
      return ctx.getLitNumber(tok.span!);
    default:
      return ctx.getLitString(tok.span!);
  }
}

function getTokenAssignOpName(op: Token): string {
  const opName = getTokenName(op);
  return opName.substring(1, opName.length - 2);
}

function getTokenOpName(op: Token): string {
  const opName = getTokenName(op);
  return opName.substring(1, opName.length - 1);
}

class Context {
  readonly script: string;
  readonly errors: ScriptError[];
  private readonly lexer: Lexer;
  private readonly expectedKinds: string[];

  constructor(script: string) {
    this.script = script;
    this.lexer = new Lexer(script);
    this.errors = [];
    this.expectedKinds = [];
  }

  curPos(): number {
    return this.lexer.curPos;
  }

  next(): Spanned<Token> {
    this.expectedKinds.length = 0;
    return this.lexer.next();
  }

  matches(kind: Token): Token | undefined {
    this.expectedKinds.push(getTokenName(kind));
    const tok = this.lexer.peek().data;
    return tok === kind ? tok : undefined;
  }

  matchesFamily<T>(family: TokenFamily<T>): Token | undefined {
    this.expectedKinds.push(family.name);
    const tok = this.lexer.peek().data;
    return family.tokens.has(tok) ? tok : undefined;
  }

  consume(kind: Token): Spanned<Token> {
    const tok = this.tryConsume(kind);
    if (tok === undefined) {
      return this.unexpected();
    }
    return tok;
  }

  consume2(kind1: Token, kind2: Token): Spanned<Token> {
    let tok = this.tryConsume(kind1);
    if (tok === undefined) {
      tok = this.tryConsume(kind2);
      if (tok === undefined) {
        return this.unexpected();
      }
    }
    return tok;
  }

  consumeFamily<T>(family: TokenFamily<T>): Spanned<Token> {
    const tok = this.tryConsumeFamily(family);
    if (tok === undefined) {
      return this.unexpected();
    }
    return tok;
  }

  unexpected(): never {
    const tok = this.lexer.peek();
    const expected = new Set([...this.expectedKinds]);
    expected.delete(getTokenName(Token.EOF));
    if (expected.size === 0) {
      throw makeScriptIncident(
        ErrorType.Syntaxic,
        `Unexpected ${getTokenName(tok.data)}`,
        tok.span,
      );
    }

    const parts = ["Unexpected ", getTokenName(tok.data), "; expected "];
    for (const kind of [...expected.values()].sort()) {
      parts.push(kind);
      parts.push(", ");
    }
    parts.pop();
    parts[parts.length - 2] = " or ";

    this.expectedKinds.length = 0;
    throw makeScriptIncident(ErrorType.Syntaxic, parts.join(""), tok.span);
  }

  tryConsume(kind: Token): Spanned<Token> | undefined {
    const tok = this.lexer.peek().data;
    if (tok === kind) {
      this.expectedKinds.length = 0;
      return this.lexer.next();
    }
    this.expectedKinds.push(getTokenName(kind));
    return undefined;
  }

  tryConsumeFamily<T>(family: TokenFamily<T>): Spanned<Token> | undefined {
    const tok = this.lexer.peek().data;
    if (family.tokens.has(tok)) {
      this.expectedKinds.length = 0;
      return this.lexer.next();
    }

    this.expectedKinds.push(family.name);
    return undefined;
  }

  getString(span: Span): Spanned<string> {
    return {
      span,
      data: this.script.substring(span.start, span.end),
    };
  }

  getLitNumber(span: Span): Spanned<ast.ScriptValue> {
    return {
      span,
      data: {
        type: "lit",
        value: parseFloat(this.getString(span).data),
      },
    };
  }

  getLitString(span: Span): Spanned<ast.ScriptValue> {
    if (span.end - span.start <= 2) {
      return { span, data: { type: "lit", value: "" } };
    }

    const strSpan = Span.of(span.start + 1, span.end - 1);
    return {
      span,
      data: {
        type: "lit",
        value: unescapeString(this.getString(strSpan).data),
      },
    };
  }

  catchError<T>(type: ErrorType | null, f: () => T): T | undefined {
    try {
      return f();
    } catch (e) {
      if (isScriptIncident(e)) {
        if (type === null || type === e.data.type) {
          this.errors.push(e.data);
          return undefined;
        }
      }

      throw e;
    }
  }
}
