import { Span, Spanned } from "./text.js";

// A visual position inside a bloc of text (0-based)
export interface TextPos {
  readonly line: number;
  readonly col: number;
}

// A map to translate a byte position to a TextPos
export class TextMap {
  readonly text: string;
  readonly lines: ReadonlyArray<number>;
  readonly tabWidth: number;

  constructor(text: string, tabWidth: number) {
    const lines = [];
    let pos = 0;
    if (text.length > 0) {
      do {
        lines.push(pos);
        pos = text.indexOf("\n", pos) + 1;
      } while (pos > 0);
    }

    this.text = text;
    this.lines = lines;
    this.tabWidth = tabWidth;
  }

  public posOf(idx: number): TextPos {
    // Clamp value
    idx = Math.min(Math.max(0, idx), this.text.length);

    // Binary search
    let min = 0;
    let max = this.lines.length;
    while (max - min > 1) {
      const mid = Math.floor((min + max) / 2);
      if (mid < this.text.length && idx < this.lines[mid]) {
        max = mid;
      } else {
        min = mid;
      }
    }

    return {
      line: min,
      col: this.computeWidth(this.lines[min], idx),
    };
  }

  // Return the span of a given line, without the trailing \n
  public getLineSpan(line: number): Span | undefined {
    if (line < 0 || line >= this.lines.length) {
      return undefined;
    }
    const start = this.lines[line];
    let end = line === this.lines.length - 1 ? this.text.length : this.lines[line + 1];
    if (this.text.charCodeAt(end - 1) === 0x0A) {
      end -= 1;
    }
    return Span.of(start, end);
  }

  // Return the given line, without the trailing \n
  public getLine(line: number): string | undefined {
    const span = this.getLineSpan(line);
    if (span === undefined) {
      return undefined;
    }
    return this.text.substring(span.start, span.end);
  }

  // Return the printable version of the given line, without the trailing \n,
  // skipping control characters and handling tabulations
  public getPrintableLine(line: number): string | undefined {
    const span = this.getLineSpan(line);
    if (span === undefined) {
      return undefined;
    }

    let width = 0;
    let str = "";
    for (let i = span.start; i < span.end; i++) {
      const c = this.text.codePointAt(i)!;
      if (c === 0x09) { // Tabulation - go to next tab stop
        const tab = this.tabWidth - (width % this.tabWidth);
        str += " ".repeat(tab);
        width += tab;
      } else if (c <= 0x1F) {
        // ASCII control chars - skip them
      } else if (c >= 0xDC00 && c < 0xE000) {
        // Low surrogates - skip, was handled in the previous iteration
      } else {
        str += String.fromCodePoint(c);
        // TODO: maybe try to handle combining & fullwidth characters ?
        width++;
      }
    }

    return str;
  }

  // Show spans within the context of their lines;
  // `lineContext` is how many lines to show before and after each span.
  public getSpansInContext(lineContext: number, ...spans: Spanned<SpanStyle>[]): string {
    if (lineContext < 0 || Math.floor(lineContext) !== lineContext) {
      throw new TypeError("lineContext must be a non-negative integer");
    }

    // COLLECT THE SPAN EVENTS
    const events: SpanEvent[] = [];
    for (const {span, data} of spans) {
      if (span === undefined) {
        continue;
      }
      const obj = {
        start: this.posOf(span.start),
        end: this.posOf(span.end),
        layout: 0,
        style: data,
      };
      if (obj.start.line === obj.end.line) {
        events.push({ span: obj, type: SpanEventType.Singleline });
      } else {
        events.push({ span: obj, type: SpanEventType.MultilineStart });
        events.push({ span: obj, type: SpanEventType.MultilineEnd });
      }
    }

    if (events.length === 0) {
      return "";
    }

    // SORT THE EVENTS FOR DISPLAY
    events.sort((a, b) => {
      const spanA = a.type === SpanEventType.MultilineEnd ? a.span.end : a.span.start;
      const spanB = b.type === SpanEventType.MultilineEnd ? b.span.end : b.span.start;

      // First, sort by line
      if (spanA.line !== spanB.line) {
        return spanA.line - spanB.line;
      }

      // We want to display multilines above singlelines, and multilines ends above starts
      if (a.type !== b.type) {
        return a.type - b.type;
      }

      const diff = spanA.col - spanB.col;
      // Singlelines and starts of multilines sort left to right
      // Ends of multilines sort right to left
      return a.type === SpanEventType.MultilineEnd ? -diff : diff;
    });

    // BUILD THE STRING
    let nextLine = 0;
    const layout = new LayoutEngine();

    const textLine = (line: number) => {
      const txt = this.getPrintableLine(line);
      if (txt !== undefined) {
        layout.textLine(line, txt);
      }
    };
    const textLines = (start: number, end: number) => {
      while (start < end) {
        textLine(start++);
      }
      return end;
    };

    for (const event of events) {
      const span = event.span;
      const line = event.type === SpanEventType.MultilineEnd ? event.span.end.line : event.span.start.line;

      // Render lines as needed
      if (line >= nextLine) {
        // Render post-ctx lines of previous event
        nextLine = textLines(nextLine, Math.min(line, nextLine + lineContext));

        // Add ellipsis if needed
        if (nextLine < line - lineContext) {
          layout.ellipsisLine();
        }

        // Render pre-ctx lines and main line of current event
        if (lineContext > 0) {
          nextLine = textLines(Math.max(nextLine, line + 1 - lineContext), line + 1);
        } else {
          textLine(line);
          nextLine += 1;
        }
      }

      // Render stuff
      switch(event.type) {
        case SpanEventType.Singleline:
          layout.singlelineSpan(span.style, span.start.col, span.end.col);
          break;
        case SpanEventType.MultilineStart:
          span.layout = layout.startMultilineSpan(span.style, span.start.col);
          break;
        case SpanEventType.MultilineEnd:
          layout.endMultilineSpan(span.layout, span.end.col);
          break;
      }
    }
    // Render final post-ctx lines
    textLines(nextLine, nextLine + lineContext);

    return layout.buildString();
  }

  // Compute the visual width of a segment of text
  // (from and to should be valid and on the same line)
  private computeWidth(from: number, to: number): number {
    let width = 0;
    for (let i = from; i < to; i++) {
      const c = this.text.codePointAt(i)!;
      if (c === 0x09) { // Tabulation - go to next tab stop
        width += this.tabWidth - (width % this.tabWidth);
      } else if (c <= 0x1F) {
        // ASCII control chars - no width
      } else if (c >= 0xDC00 && c < 0xE000) {
        // Low surrogates - skip, was handled in the previous iteration
      } else {
        // TODO: maybe try to handle combining & fullwidth characters ?
        width++;
      }
    }

    return width;
  }
}

enum SpanEventType {
  // The order correspond to the display order
  MultilineEnd = 1,
  MultilineStart = 2,
  Singleline = 3,
}

interface SpanEvent {
  span: {
    start: TextPos;
    end: TextPos;
    layout: number;
    style: SpanStyle;
  };
  type: SpanEventType;
}

export enum SpanStyle {
  None,
  Light,
  Heavy,
  Double,
  Curved,
  Dotted,
}

export interface SpanStyleInfo {
  readonly id: SpanStyle;
  readonly singleline: string;
  readonly ending: string;
  readonly vertical: string[];
  readonly turnTop: string;
  readonly turnBottom: string;
}

export const STYLES: ReadonlyArray<SpanStyleInfo> = [{
  id: SpanStyle.None,
  singleline: " ",
  ending: " ",
  vertical: [" ", "─", "━", "═", "─", "╌"],
  turnTop: " ",
  turnBottom: " ",
}, {
  id: SpanStyle.Light,
  singleline: "~",
  ending: "^",
  vertical: ["│", "┼", "┿", "╪", "┼", "┼"],
  turnTop: "┌",
  turnBottom: "└",
}, {
  id: SpanStyle.Heavy,
  singleline: "▲",
  ending: "^",
  vertical: ["┃", "╂", "╋", "╪", "╂", "╂"],
  turnTop: "┏",
  turnBottom: "┗",
}, {
  id: SpanStyle.Double,
  singleline: "^",
  ending: "^",
  vertical: ["║", "╫", "╫", "╬", "╫", "╫"],
  turnTop: "╔",
  turnBottom: "╚",
}, {
  id: SpanStyle.Curved,
  singleline: "~",
  ending: "^",
  vertical: ["│", "┼", "┿", "╪", "┼", "┼"],
  turnTop: "╭",
  turnBottom: "╰",
}, {
  id: SpanStyle.Dotted,
  singleline: "-",
  ending: "^",
  vertical: ["╎", "─", "━", "═", "─", "╌"],
  turnTop: "┌",
  turnBottom: "└",
}];

class LayoutEngine {
  private static LINE_PARTS_COUNT: number = 5;
  private static SEPARATOR: string = " : ";
  private static SEPARATOR_EMPTY: string = "   ";
  private static SEPARATOR_LEN: number = 3;

  private parts: string[];

  private maxLineNumberLen: number = 0;
  private maxMultilinesStrLen: number = 1;

  private curMultilines: SpanStyle[];
  private curMultilinesStr: string | null = " ";

  private updatableLines: number = 0;

  constructor() {
    this.parts = [];
    this.curMultilines = [];
  }

  public textLine(lineNumber: number, line: string): void {
    this.updatableLines = 0;
    this.rawLine(lineNumber.toString() + " ", null, LayoutEngine.SEPARATOR, line);
  }

  public ellipsisLine(): void {
    this.updatableLines = 0;
    this.rawLine("", null, LayoutEngine.SEPARATOR_EMPTY, "...");
  }

  public singlelineSpan(style: SpanStyle, from: number, to: number): void {
    // Find first free text line
    let partIdx = this.parts.length - LayoutEngine.LINE_PARTS_COUNT * this.updatableLines + 3;
    for (; partIdx < this.parts.length; partIdx += LayoutEngine.LINE_PARTS_COUNT) {
      const l = this.parts[partIdx];
      if (from === 0 ? l.length === 0 : l.length < from) {
        break;
      }
    }
    if (partIdx >= this.parts.length) {
      this.rawLine("", null, LayoutEngine.SEPARATOR_EMPTY, "");
      this.updatableLines++;
    }

    let line = this.parts[partIdx];
    line = line.padEnd(from, " ");
    line = line.padEnd(to, STYLES[style].singleline);
    this.parts[partIdx] = line;
  }

  public startMultilineSpan(style: SpanStyle, from: number): number {
    // Find first free slot
    let spanId = this.curMultilines.findIndex(s => s === SpanStyle.None);
    if (spanId < 0) {
      spanId = this.curMultilines.length;
      this.curMultilines.push(SpanStyle.None);
    }

    const multilines = this.makeMultilinesStr(style, spanId, true);
    const horizontal = STYLES[SpanStyle.None].vertical[style];
    const sep = horizontal.repeat(LayoutEngine.SEPARATOR_LEN);
    const text = horizontal.repeat(from) + STYLES[style].ending;
    this.rawLine("", multilines, sep, text);
    this.updatableLines++;

    this.curMultilines[spanId] = style;
    this.curMultilinesStr = null;
    return spanId;
  }

  public endMultilineSpan(spanId: number, to: number): void {
    const style = this.curMultilines[spanId];
    this.curMultilines[spanId] = SpanStyle.None;
    this.curMultilinesStr = null;

    const multilines = this.makeMultilinesStr(style, spanId, false);
    const horizontal = STYLES[SpanStyle.None].vertical[style];
    const sep = horizontal.repeat(LayoutEngine.SEPARATOR_LEN);
    const text = horizontal.repeat(Math.max(0, to - 1)) + STYLES[style].ending;
    this.rawLine("", multilines, sep, text);
    this.updatableLines++;
  }

  public buildString(): string {
    this.updatableLines = 0;
    for (let i = 0; i < this.parts.length; i += LayoutEngine.LINE_PARTS_COUNT) {
      const c = this.parts[i + 2].charAt(0);
      this.parts[i + 0] = this.parts[i + 0].padStart(this.maxLineNumberLen, " ");
      this.parts[i + 1] = this.parts[i + 1].padEnd(this.maxMultilinesStrLen, c);
    }
    return this.parts.join("");
  }

  private makeMultilinesStr(crossing: SpanStyle, turn: number, isTop: boolean): string {
    let str = " ";
    let crossingActive = false;
    const crossingInfo = STYLES[crossing];
    for (let i = 0; i < this.curMultilines.length; i++) {
      const cur = STYLES[this.curMultilines[i]];
      if (i === turn) {
        str += isTop ? crossingInfo.turnTop : crossingInfo.turnBottom;
        crossingActive = true;
      } else {
        str += cur.vertical[crossingActive ? crossing : SpanStyle.None];
      }
    }
    return str;
  }

  private rawLine(lineNumber: string, multilines: string | null, separator: string, text: string): void {
    // Recalculate the multiline prefix if needed
    if (this.curMultilinesStr === null) {
      this.curMultilinesStr = this.makeMultilinesStr(SpanStyle.None, -1, false);
      this.maxMultilinesStrLen = Math.max(this.maxMultilinesStrLen, this.curMultilinesStr.length);
    }

    this.maxLineNumberLen = Math.max(this.maxLineNumberLen, lineNumber.length);
    if (multilines === null) {
      multilines = this.curMultilinesStr!;
    } else {
      this.maxMultilinesStrLen = Math.max(this.maxMultilinesStrLen, multilines.length);
    }

    this.parts.push(lineNumber, multilines, separator, text, "\n");
  }
}
