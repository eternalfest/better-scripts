import { Incident } from "incident";

import { Span } from "./text.js";

export enum ErrorType {
  Lexical,
  Syntaxic,
  Semantic,
}

export interface ScriptError {
  readonly type: ErrorType;
  readonly msg: string;
  readonly spans: ReadonlyArray<Span>;
}

export function makeScriptIncident(
  type: ErrorType,
  msg: string,
  ...spans: (Span | undefined)[]
): Incident<ScriptError, "ScriptError"> {

  const spanList = [];
  for (const span of spans) {
    if (span !== undefined) {
      spanList.push(span);
    }
  }

  return Incident("ScriptError", { type, msg, spans: spanList });
}

export function isScriptIncident(e: any): boolean {
  return e instanceof Incident && e.name === "ScriptError";
}
