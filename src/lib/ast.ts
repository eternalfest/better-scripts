
import { Incident } from "incident";

import { ErrorType, makeScriptIncident } from "./error.js";
import { Spanned } from "./text.js";

// A script expression
export type ScriptExpr = ScriptValue | ScriptExprBinOp | ScriptExprUnaryOp | ScriptExprCall | ScriptExprVar;

// A basic script value
export interface ScriptValue {
  readonly type: "lit";
  value: boolean | number | string;
}

// An infix binary operation
export interface ScriptExprBinOp {
  readonly type: "binop";
  op: string;
  left: Spanned<ScriptExpr>;
  right: Spanned<ScriptExpr>;
}

// A prefix unary operation
export interface ScriptExprUnaryOp {
  readonly type: "unaryop";
  op: string;
  expr: Spanned<ScriptExpr>;
}

// A function call
export interface ScriptExprCall {
  readonly type: "call";
  func: string;
  args: ReadonlyArray<Spanned<ScriptExpr>>;
}

// A variable access
export interface ScriptExprVar {
  readonly type: "var";
  name: Spanned<string>;
  parts: ReadonlyArray<Spanned<string | ScriptExpr>>;
}

function* exportScriptVarParts(expr: ScriptExprVar): any {
  yield expr.name.data;
  for (const part of expr.parts) {
    if (typeof part.data === "string") {
      yield part.data;
    } else {
      yield* exportScriptExprParts(part.data);
    }
  }
}

function* exportScriptExprParts(expr: ScriptExpr): any {
  switch (expr.type) {
    case "lit":
      yield expr.value;
      break;
    case "binop":
      yield* exportScriptExprParts(expr.left.data);
      yield* exportScriptExprParts(expr.right.data);
      yield { call: expr.op, args: 2 };
      break;
    case "unaryop":
      if (expr.op === "-") {
        // Special case: '-' is actually a binary op so we need to export it as such
        yield 0;
        yield* exportScriptExprParts(expr.expr.data);
        yield { call: "-", args: 2 };
      } else {
        yield* exportScriptExprParts(expr.expr.data);
        yield { call: expr.op, args: 1 };
      }
      break;
    case "call":
      for (const arg of expr.args) {
        yield* exportScriptExprParts(arg.data);
      }
      yield { call: expr.func, args: expr.args.length };
      break;
    case "var":
      yield* exportScriptVarParts(expr);
      yield { get: true, args: expr.parts.length + 1 };
      break;
    default:
      throw new Incident("InvalidExprType", `Unsupported expr type: ${(expr as ScriptExpr).type}`);
  }
}

function exportScriptExpr(expr: ScriptExpr): any {
  if (expr.type === "lit") {
    return expr.value;
  }
  return [...exportScriptExprParts(expr)];
}

// A key = value pair
export interface ScriptParameter {
  readonly name: Spanned<string>;
  value: Spanned<ScriptExpr>;
}

function makeDict<T, U>(map: Map<string, T>, mapper: (e: T) => U): any {
  const obj: any = {};
  for (const [k, v] of map) {
    obj[k] = mapper(v);
  }
  return obj;
}

// A script node
export type ScriptNode = ScriptEvent | ScriptAssign;

export interface ScriptAssign {
  readonly type: "assign";
  lvalue: Spanned<ScriptExprVar>;
  op: string | undefined;
  rvalue: Spanned<ScriptExpr>;
}

// A script event (or trigger)
export class ScriptEvent {
  public readonly type: "event" = "event";
  public readonly name: Spanned<string>;
  parameters: Map<string, ScriptParameter>;
  modifiers: Map<string, ScriptParameter>;
  anonParameter: Spanned<ScriptExpr> | undefined;
  public children: Spanned<ScriptNode>[];

  constructor(name: Spanned<string>) {
    this.name = name;
    this.parameters = new Map();
    this.modifiers = new Map();
    this.children = [];
  }

  public getParameter(name: string): ScriptParameter | undefined {
    return this.parameters.get(name);
  }

  public getAnonParameter(): Spanned<ScriptExpr> | undefined {
    return this.anonParameter;
  }

  public setParameter(param: ScriptParameter): ScriptParameter | undefined {
    if (this.anonParameter !== undefined) {
      throw makeScriptIncident(
        ErrorType.Semantic,
        "Can't have named paremeters if anonymous parameter already exists",
        param.name.span, this.anonParameter.span,
      );
    }
    const old = this.parameters.get(param.name.data);
    this.parameters.set(param.name.data, param);
    return old;
  }

  public setAnonParameter(param: Spanned<ScriptExpr> | undefined): Spanned<ScriptExpr> | undefined {
    if (param !== undefined && this.parameters.size !== 0) {
      throw makeScriptIncident(
        ErrorType.Semantic,
        "Can't have anonymous paremeter if named parameters already exist",
        param.span,
      );
    }
    const old = this.anonParameter;
    this.anonParameter = param;
    return old;
  }

  public addParameter(param: ScriptParameter): void {
    const old = this.getParameter(param.name.data);
    if (old !== undefined) {
      throw makeScriptIncident(
        ErrorType.Semantic,
        `Duplicate parameter name: ${param.name.data}`,
        param.name.span, old.name.span,
      );
    }
    this.setParameter(param);
  }

  public removeParameter(name: string): ScriptParameter | undefined {
    const old = this.parameters.get(name);
    this.parameters.delete(name);
    return old;
  }

  public getModifier(name: string): ScriptParameter | undefined {
    return this.modifiers.get(name);
  }

  public setModifier(modifier: ScriptParameter): ScriptParameter | undefined {
    const name = modifier.name.data;
    switch (name) {
      case "children":
      case "params":
      case "name":
        throw makeScriptIncident(
          ErrorType.Semantic,
          `Invalid modifier name: ${name}`,
          modifier.name.span,
        );

      default: {
        if (modifier.value.data.type !== "lit") {
          throw makeScriptIncident(
            ErrorType.Semantic,
            "Modifiers must be literal values",
            modifier.value.span,
          );
        }

        const old = this.modifiers.get(name);
        this.modifiers.set(name, modifier);
        return old;
      }
    }
  }

  public addModifier(modifier: ScriptParameter): void {
    const old = this.getModifier(modifier.name.data);
    if (old !== undefined) {
      throw makeScriptIncident(
        ErrorType.Semantic,
        `Duplicate modifier name: ${modifier.name.data}`,
        modifier.name.span, old.name.span,
      );
    }
    this.setModifier(modifier);
  }

  public removeModifier(name: string): ScriptParameter | undefined {
    const old = this.modifiers.get(name);
    this.modifiers.delete(name);
    return old;
  }
}

function exportScriptNode(node: ScriptNode): any {
  switch (node.type) {
    case "event": {
      const obj = makeDict(node.modifiers, m => exportScriptExpr(m.value.data));
      obj.name = node.name.data;
      if (node.anonParameter !== undefined) {
        obj.params = exportScriptExpr(node.anonParameter.data);
      } if (node.parameters.size > 0) {
        obj.params = makeDict(node.parameters, p => exportScriptExpr(p.value.data));
      }
      if (node.children.length > 0) {
        obj.children = node.children.map(c => exportScriptNode(c.data));
      }
      return obj;

    }
    case "assign":
      return {
        name: "exec",
        params: [
          ...exportScriptVarParts(node.lvalue.data),
          ...exportScriptExprParts(node.rvalue.data),
          {
            set: node.op === undefined ? true : node.op,
            args: node.lvalue.data.parts.length + 2,
          },
        ],
      };
    default:
      throw new Incident("InvalidNodeType", `Unsupported node type: ${(node as ScriptNode).type}`);
  }
}

export class BetterScript {
  public nodes: Spanned<ScriptNode>[];

  constructor() {
    this.nodes = [];
  }

  public version(): number {
    return 1;
  }

  public export(): any {
    return {
      version: this.version(),
      nodes: this.nodes.map(n => exportScriptNode(n.data)),
    };
  }
}
