# Next

- **[Internal]** Rework project structure.
- **[Internal]** Update to Yarn 2.

# 0.3.0 (2020-06-29)

- **[Breaking change]** Update to ESM.

# 0.2.1 (2019-12-31)

- **[Internal]** Update dev dependencies.

# 0.2.0 (2019-04-28)

- **[Breaking change]** Spans can be styled independently in `TextMap.getSpansInContext`.
- **[Feature]** Add support for expression and assignment syntax.
- **[Feature]** Add more spans styles for `TextMap`.

# 0.1.0 (2019-01-03)

- **[Feature]** First release.
